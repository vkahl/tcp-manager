use std::{
    io::{self, Read, Write},
    net::{IpAddr, SocketAddr, TcpStream},
    thread,
    time::Duration,
};
use tcp_manager::*;

#[derive(Debug, Clone)]
struct TestType(usize, IpAddr);

fn test_handler(peer_ip: IpAddr, mut stream: TcpStream) -> Option<TestType> {
    // this only fails if a duration of zero is passed in
    stream
        .set_read_timeout(Some(Duration::from_millis(2000)))
        .unwrap();
    let mut s = String::new();
    let bytes_read = stream.read_to_string(&mut s).ok()?;
    Some(TestType(bytes_read, peer_ip))
}

fn test_sender(peer_addr: &SocketAddr, data: &[u8]) -> io::Result<()> {
    let mut conn = TcpStream::connect(peer_addr)?;
    conn.set_write_timeout(Some(Duration::from_secs(2)))?;
    conn.write_all(data)
}

fn main() {
    let mut tm = TcpManagerBuilder::default()
        .worker_idle_timeout(Duration::from_secs(7))
        .build(&test_handler, &test_sender);
    let rx = tm.take_rx().unwrap();
    thread::spawn(move || {
        tm.listen("0.0.0.0:1234".parse().unwrap()).unwrap();
    });
    thread::spawn(|| {
        let tm = TcpManagerBuilder::default()
            .only_send_latest()
            .worker_idle_timeout(Duration::from_secs(5))
            .build(&test_handler, &test_sender);
        let data: Vec<_> = b"TEST DATA".to_vec();
        let peer_addr = "127.0.0.1:1234".parse().unwrap();
        thread::sleep(Duration::from_millis(1000));
        tm.send(peer_addr, data[..1].to_vec());
        thread::sleep(Duration::from_millis(1000));
        tm.send(peer_addr, data[..2].to_vec());
        tm.send(peer_addr, data[..3].to_vec());
        tm.send(peer_addr, data[..4].to_vec());
        thread::sleep(Duration::from_millis(6000));
        tm.send(peer_addr, data[..5].to_vec());
        thread::sleep(Duration::from_millis(8000));
        tm.send(peer_addr, data[..6].to_vec());
        tm.send(peer_addr, data[..7].to_vec());
        tm.send(peer_addr, data[..8].to_vec());
        tm.send(peer_addr, data[..9].to_vec());
    });
    while let Ok(msg) = rx.recv() {
        println!("{msg:?}");
    }
}
