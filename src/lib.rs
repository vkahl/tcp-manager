use log::*;
use parking_lot::RwLock;
use std::{
    collections::HashMap,
    io,
    net::{IpAddr, SocketAddr, TcpListener, TcpStream},
    sync::{
        mpsc::{sync_channel as bounded, Receiver, SyncSender as Sender},
        Arc,
    },
    thread,
    time::Duration,
};

pub struct TcpManagerBuilder {
    manager_bound: usize,
    worker_bound: usize,
    worker_idle_timeout: Duration,
    only_send_latest: bool,
}

impl Default for TcpManagerBuilder {
    fn default() -> Self {
        Self {
            manager_bound: 512,
            worker_bound: 64,
            worker_idle_timeout: Duration::from_secs(10),
            only_send_latest: false,
        }
    }
}

impl TcpManagerBuilder {
    pub fn manager_bound(mut self, manager_bound: usize) -> Self {
        self.manager_bound = manager_bound;
        self
    }
    pub fn worker_bound(mut self, worker_bound: usize) -> Self {
        self.worker_bound = worker_bound;
        self
    }
    pub fn worker_idle_timeout(mut self, worker_idle_timeout: Duration) -> Self {
        self.worker_idle_timeout = worker_idle_timeout;
        self
    }
    pub fn only_send_latest(mut self) -> Self {
        self.only_send_latest = true;
        self
    }
    pub fn build<T, H, S>(
        self,
        handler_function: &'static H,
        send_function: &'static S,
    ) -> TcpManager<T, H, S>
    where
        T: 'static + Send,
        H: Fn(IpAddr, TcpStream) -> Option<T> + 'static + Sync,
        S: Fn(&SocketAddr, &[u8]) -> io::Result<()> + 'static + Sync,
    {
        TcpManager::new(
            handler_function,
            send_function,
            self.manager_bound,
            self.worker_bound,
            self.worker_idle_timeout,
            self.only_send_latest,
        )
    }
}

pub struct TcpManager<T, H, S>
where
    T: 'static + Send,
    H: Fn(IpAddr, TcpStream) -> Option<T> + 'static + Sync,
    S: Fn(&SocketAddr, &[u8]) -> io::Result<()> + 'static + Sync,
{
    handler_function: &'static H,
    send_function: &'static S,
    manager_tx: Sender<T>,
    manager_rx: Option<Receiver<T>>,
    worker_bound: usize,
    worker_idle_timeout: Duration,
    receiver_tx_map: Arc<RwLock<HashMap<IpAddr, Sender<TcpStream>>>>,
    sender_tx_map: Arc<RwLock<HashMap<SocketAddr, Sender<Vec<u8>>>>>,
    only_send_latest: bool,
}

impl<T, H, S> TcpManager<T, H, S>
where
    T: 'static + Send,
    H: Fn(IpAddr, TcpStream) -> Option<T> + 'static + Sync,
    S: Fn(&SocketAddr, &[u8]) -> io::Result<()> + 'static + Sync,
{
    pub fn new(
        handler_function: &'static H,
        send_function: &'static S,
        manager_bound: usize,
        worker_bound: usize,
        worker_idle_timeout: Duration,
        only_send_latest: bool,
    ) -> Self {
        let (manager_tx, manager_rx) = bounded(manager_bound);
        Self {
            handler_function,
            send_function,
            manager_tx,
            manager_rx: Some(manager_rx),
            worker_bound,
            worker_idle_timeout,
            receiver_tx_map: Arc::new(RwLock::new(HashMap::new())),
            sender_tx_map: Arc::new(RwLock::new(HashMap::new())),
            only_send_latest,
        }
    }

    pub fn listen(&self, addr: SocketAddr) -> io::Result<()> {
        let listener = TcpListener::bind(addr)?;
        for stream in listener.incoming() {
            debug!("Incomming message from {}", addr);
            self.handle_incoming(stream).ok();
        }
        Ok(())
    }

    fn handle_incoming(&self, stream: io::Result<TcpStream>) -> io::Result<()> {
        let stream = stream?;
        let peer_ip = stream.peer_addr()?.ip();
        debug!("Handling stream from {}", peer_ip);
        let stream = if let Some(tx) = self.receiver_tx_map.read().get(&peer_ip) {
            #[cfg(not(feature = "channel-crossbeam"))]
            match tx.send(stream) {
                Ok(()) => return Ok(()),
                Err(e) => e.0,
            }
            #[cfg(feature = "channel-crossbeam")]
            match tx.send(stream) {
                Ok(()) => return Ok(()),
                Err(e) => e.into_inner(),
            }
        } else {
            stream
        };
        // apparently there is no active receiver for that `peer_ip` -> create a new one
        let tx = self.add_receiver(peer_ip);
        // this can't fail because the thread didn't handle a single message yet
        tx.send(stream).unwrap();
        Ok(())
    }

    fn add_receiver(&self, peer_ip: IpAddr) -> Sender<TcpStream> {
        let (tx, rx) = bounded(self.worker_bound);
        let manager_tx_clone = self.manager_tx.clone();
        let handler_function = self.handler_function;
        let worker_timeout = self.worker_idle_timeout;
        let receiver_tx_map_clone = self.receiver_tx_map.clone();
        debug!("Spawning new receiver thread for {}", peer_ip);
        thread::spawn(move || {
            loop {
                if let Ok(stream) = rx.recv_timeout(worker_timeout) {
                    if let Some(value) = (handler_function)(peer_ip, stream) {
                        if manager_tx_clone.send(value).is_ok() {
                            debug!(
                                "Successfully run handler function for message from {}",
                                peer_ip
                            );
                            continue;
                        }
                    }
                }

                // something failed or timed out ->

                // acquire an exclusive lock over the receiver map
                let mut map_lock = receiver_tx_map_clone.write();

                // check for messages that got put into the channel right before acquiring the
                // lock
                if let Ok(stream) = rx.try_recv() {
                    // there has been a message -> handle it and continue (receiver is still active)
                    drop(map_lock);
                    if let Some(value) = (handler_function)(peer_ip, stream) {
                        if manager_tx_clone.send(value).is_ok() {
                            continue;
                        }
                    }
                } else {
                    // there hasn't been a new message -> receiver is not active anymore
                    // drop `rx`
                    drop(rx);
                    // remove `tx` from sender map
                    debug!("Removing receiver for {}", peer_ip);
                    map_lock.remove(&peer_ip);
                    // release the `map_lock` by returning
                    return;
                }
            }
        });
        self.receiver_tx_map.write().insert(peer_ip, tx.clone());
        tx
    }

    fn add_sender(&self, peer_addr: SocketAddr) -> Sender<Vec<u8>> {
        let (tx, rx): (Sender<Vec<u8>>, Receiver<Vec<u8>>) = bounded(self.worker_bound);
        let worker_timeout = self.worker_idle_timeout;
        let send_function = self.send_function;
        let sender_tx_map_clone = self.sender_tx_map.clone();
        let only_send_latest = self.only_send_latest;
        debug!("Spawning new sender thread for {}", peer_addr);
        thread::spawn(move || {
            loop {
                let data = if only_send_latest {
                    rx.try_iter()
                        .last()
                        .or_else(|| rx.recv_timeout(worker_timeout).ok())
                } else {
                    rx.recv_timeout(worker_timeout).ok()
                };

                if let Some(data) = data {
                    match (send_function)(&peer_addr, &data) {
                        Ok(_) => {
                            debug!(
                                "Successfully run send function for message to {}",
                                peer_addr
                            )
                        }
                        Err(e) => {
                            debug!("Send function failed for message to {} ({})", peer_addr, e)
                        }
                    };
                    continue;
                }

                // no new messages to send for a time span of `worker_timeout`

                // acquire an exclusive lock over the sender map
                let mut map_lock = sender_tx_map_clone.write();

                // check for messages that got put into the channel right before acquiring the
                // lock
                if let Ok(data) = rx.try_recv() {
                    // there has been a message -> handle it and continue (sender is still active)
                    drop(map_lock);
                    if (send_function)(&peer_addr, &data).is_ok() {
                        continue;
                    }
                } else {
                    // there hasn't been a new message -> sender is not active anymore
                    // drop `rx`
                    drop(rx);
                    // remove `tx` from sender map
                    debug!("Removing sender for {}", peer_addr);
                    map_lock.remove(&peer_addr);
                    // release the `map_lock` by returning
                    return;
                }
            }
        });
        self.sender_tx_map.write().insert(peer_addr, tx.clone());
        tx
    }

    pub fn send(&self, addr: SocketAddr, data: Vec<u8>) {
        let data = if let Some(tx) = self.sender_tx_map.read().get(&addr) {
            #[cfg(not(feature = "channel-crossbeam"))]
            match tx.send(data) {
                Ok(()) => return,
                Err(e) => e.0,
            }
            #[cfg(feature = "channel-crossbeam")]
            match tx.send(data) {
                Ok(()) => return,
                Err(e) => e.into_inner(),
            }
        } else {
            data
        };
        // apparently there is no active sender for that `addr` -> create a new one
        let tx = self.add_sender(addr);
        // this can't fail because the thread didn't handle a single message yet
        tx.send(data).unwrap();
    }

    pub fn rx(&self) -> Option<&Receiver<T>> {
        self.manager_rx.as_ref()
    }

    pub fn take_rx(&mut self) -> Option<Receiver<T>> {
        self.manager_rx.take()
    }
}
